package com.company;
import java.lang.reflect.Field;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

public class Main {

    public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException, InstantiationException {
        Test test1 = new Test();
        System.out.println(test1.sum(3,4));
        System.out.println(test1.tell("Hellow  "));
        test1.sum(24,3);
        test1.tell("this");
        test1.setX(4);
        System.out.println(test1.getX());
        Class test = test1.getClass();
        Constructor[] constructor = test.getDeclaredConstructors();
        for (Constructor s : test.getDeclaredConstructors()) {
            System.out.println(s);
        }
        Field[] field = test.getDeclaredFields();
        for (Field s : test.getDeclaredFields()) {
            System.out.println(s);
        }
        Method[] methods = test.getDeclaredMethods();
        for (Method s : test.getDeclaredMethods()) {
            System.out.println(s);
        }
        Test test2 = (Test)test.newInstance();
        System.out.println(test2);


    }
}