package com.company;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Robot robot = new Robot();
        robot.work();
        CoffeRobot coffeRobot = new CoffeRobot();
        coffeRobot.work();
        RobotDancer robotDancer = new RobotDancer();
        robotDancer.work();
        RobotCoocker robotCoocker = new RobotCoocker();
        robotCoocker.work();
        Robot[] array = new Robot[3];
        array[0]= coffeRobot;
        array[1]= robotCoocker;
        array[2]= robotDancer;
        for (int i = 0; i>array.length; i++);
        System.out.println(Arrays.toString(array));

    }
}
