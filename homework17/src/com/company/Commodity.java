package com.company;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Commodity {

    public static void main(String[] args) {
        List<Product> productList = new LinkedList<>();
        String s;

        Scanner scanner = new Scanner(System.in);
        for (int i =0;;i++){
            System.out.println("Enter what you need!");
            s = scanner.nextLine();

            switch (s) {
                case "addP":
                    productList.add(new Product(scanner.nextLine(), scanner.nextInt(), scanner.nextInt(), scanner.nextInt()));
                    break;
                case "removeP":
                    productList.remove(scanner);
                case "sortByName":
                    Comparator<Product> sortName = new Comparator<Product>() {
                        @Override
                        public int compare(Product product, Product t1) {
                            return product.getName().compareTo(t1.getName());
                        }
                    } ;
                    productList.sort(sortName);
                case "sortByLength":
                    Comparator<Product> sortLength = new Comparator<Product>() {
                        @Override
                        public int compare(Product product, Product t1) {
                            return product.getLength()-t1.getLength();
                        }
                    };
                case "sortByWeight":
                    Comparator<Product> sortWeight = new Comparator<Product>() {
                        @Override
                        public int compare(Product product, Product t1) {
                            return product.getWeight()-t1.getWeight();
                        }
                    };
                case "sortByWidth":
                    Comparator<Product> sortWidth = new Comparator<Product>() {
                        @Override
                        public int compare(Product product, Product t1) {
                            return product.getWidth()-t1.getWidth();
                        }
                    };
                case "getList":
                    System.out.println(productList);
                case "exit":
                    System.exit(0);
            }

        }
    }
}