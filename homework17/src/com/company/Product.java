package com.company;

public class Product {
    private String name;
    private int length;
    private int weight;
    private int width;

    public String getName()
    {
        return name;
    }

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", length=" + length +
                ", weight=" + weight +
                ", width=" + width +
                '}';
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public Product(String name, int length, int weight, int width) {
        this.name = name;
        this.length = length;
        this.weight = weight;
        this.width = width;
    }
}
