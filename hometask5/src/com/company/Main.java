package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] array = new int[10];
        for (int i = 0; i < array.length; i++) {
            System.out.println("ENTER Array[" + i + "] = ");
            array[i] = scanner.nextInt();
        }
        System.out.println("масив = ");
        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i]);
        }

        System.out.println("масив навпаки = ");
        for (int i = 9; i > 0; i--) {
            System.out.println(array[i]);
        }
    }
}
