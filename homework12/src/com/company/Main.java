package com.company;

import java.util.LinkedList;
import java.util.*;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = null;
        List<String> list = new LinkedList<>();
        do {
            if (s != null) {
                list.add(s);
            }
            System.out.println("Enter your string ");
            s = scanner.nextLine();
        } while (!s.equalsIgnoreCase("break"));

        System.out.println("Start from 's' :");

        for (String currentString : list)
        {
            if (currentString.charAt(0)=='s') {
                System.out.println(currentString);
            }
        }
        System.out.println("more than 5 :");
        for (String currentString : list) {
            if (currentString.length() > 5) {
                System.out.println(currentString);
            }
        }
    }
}
