package com.company;

import java.util.Scanner;

public class Main {
    Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        String str = "I like Java !!!";
        char result = str.charAt(14);
        System.out.println(result);
        System.out.println("********\nNext Metod\n**********");
        boolean end = str.endsWith("!!!");
        System.out.println("Закінчується на - !!! - " + end);
        System.out.println("********\nNext Metod\n**********");
        boolean start = str.startsWith("I like");
        System.out.println("Починається з - I like - " + start);
        System.out.println("********\nNext Metod\n**********");
        String sub = str.substring(7,11);
        System.out.println(sub);
        System.out.println("********\nNext Metod\n**********");
        int index = str.indexOf("Java");
        System.out.println(index);
        System.out.println("********\nNext Metod\n**********");
        String rep = str.replaceAll("a","o");
        System.out.println(rep);
        System.out.println("********\nNext Metod\n**********");
        String up = str.toUpperCase();
        System.out.println(up);
        System.out.println("********\nNext Metod\n**********");
        String down = str.toLowerCase();
        System.out.println(down);
        System.out.println("********\nNext Metod\n**********");
        String sub_ = str.substring(7,11);
        System.out.println(sub_);
        System.out.println("********\nEND\n**********");

    }
}
