package com.company;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double m;
        double n;
        int k;
        double sum;
        System.out.print("Сума вкладу = ");
        m = scanner.nextDouble();
        System.out.print("Відсоткова ставка = ");
        n = scanner.nextDouble();
        System.out.print("Кількість років  = ");
        k = scanner.nextInt();
        sum = m*Math.pow((1+n/100),k);
        System.out.println("Ваша сума за " + k + " років = " + sum);

    }
}