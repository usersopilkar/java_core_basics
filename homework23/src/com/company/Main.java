package com.company;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
        List<People> people = new LinkedList<>();
        people.add(new People("Sergiy", "man", 70));
        people.add(new People("Oleg", "man", 56));
        people.add(new People("Uliana", "woman", 34));
        people.add(new People("Rostyk", "man", 43));
        people.add(new People("Zoriana", "woman", 17));
        people.add(new People("Pavlo", "man", 80));
        people.add(new People("Roksolana", "woman", 19));

        People max = people.stream().max(Comparator.comparingInt(People::getAge)).get();
        System.out.println("Old :" + max);
        People min = people.stream().min(Comparator.comparingInt(People::getAge)).get();
        System.out.println("Young :" + min);
        people.stream().filter(person -> person.getAge() < 27 && person.getAge() > 18 && person.getSex().equals("man")).forEach(System.out::println);
        long count1 = people.stream().filter(person -> person.getAge() < 60 && person.getAge() >= 18 && person.getSex().equals("man")).count();
        long count2 = people.stream().filter(person -> person.getAge() < 55 && person.getAge() >= 18 && person.getSex().equals("woman")).count();
        System.out.println("How many workers " + (count2 + count1));
        long man = people.stream().filter(person -> person.getSex().equals("man")).count();
        long woman = people.stream().filter(person -> person.getSex().equals("woman")).count();
        System.out.println("How many man " + man);
        System.out.println("How many woman " + woman);
        long startA = people.stream().filter(person->person.getName().startsWith("A") && person.getSex().equals("woman")).count();
        System.out.println(startA);

    //People middleAge = people.stream().filter(person->person.getSex().equals("man")).reduce((s1,s2)->s1.getAge()+s2.getAge()).orElse(null);
    //long countman = people.stream().filter(person->person.getSex().equals("man")).count();
      //  System.out.println("Middle count man " + (middleAge/countman));
    }
}
