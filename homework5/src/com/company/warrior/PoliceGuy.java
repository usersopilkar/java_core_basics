package com.company.warrior;

import com.company.weapon.Gun;
import com.company.weapon.Weapon;

public class PoliceGuy extends Warrior{

    public PoliceGuy() {
        super(600, new Gun());
    }

    @Override
    public String toString() {
        return "PoliceGuy Hp:" + getHp();
    }
}
