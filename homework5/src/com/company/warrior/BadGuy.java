package com.company.warrior;

import com.company.weapon.Knife;
import com.company.weapon.Weapon;

public class BadGuy extends Warrior{
    public BadGuy() {
        super(1000, new Knife());
    }

    @Override
    public String toString() {
        return "BadGuy Hp:" + getHp();
    }
}
