package com.company.warrior;

import com.company.weapon.Weapon;

public abstract class Warrior {
    private int hp;
    private Weapon weapon;

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    public Warrior(int hp, Weapon weapon) {
        this.hp = hp;
        this.weapon = weapon;
    }

    public void attack(Warrior warrior2) {
        int damage = (int) (weapon.getDamage() * (Math.random()*weapon.getRange()*2 - weapon.getRange()));

if (Math.random()*100<weapon.getAccurency()){
    System.out.println(this + "give " + damage + "damage for " + warrior2);
    warrior2.setHp(warrior2.getHp()-damage);
}
    }
}
