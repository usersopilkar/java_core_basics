package com.company.warrior;

import com.company.weapon.Katana;
import com.company.weapon.Weapon;

public class Samurai extends Warrior {
    public Samurai() {
        super(800,new Katana());
    }

    @Override
    public String toString() {
        return "Samurai Hp:" + getHp();
    }
}
