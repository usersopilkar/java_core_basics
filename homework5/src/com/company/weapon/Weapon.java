package com.company.weapon;

public class Weapon {
    private int damage;
    private int range;
    private int accurency;

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public int getRange() {
        return range;
    }

    public void setRange(int range) {
        this.range = range;
    }

    public int getAccurency() {
        return accurency;
    }

    public void setAccurency(int accurency) {
        this.accurency = accurency;
    }

    public Weapon(int damage, int range, int accurency) {
        this.damage = damage;
        this.range = range;
        this.accurency = accurency;
    }
}
