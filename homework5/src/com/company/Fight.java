package com.company;

import com.company.warrior.BadGuy;
import com.company.warrior.PoliceGuy;
import com.company.warrior.Samurai;
import com.company.warrior.Warrior;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Fight {
    List<Warrior> warriors;

    public Fight(List<Warrior> warriors) {
        this.warriors = warriors;
    }

    public static void main(String[] args) {
        Fight fight = new Fight(new LinkedList<>(Arrays.asList(new Samurai(), new BadGuy(), new PoliceGuy())));
        fight.startFight();
    }

    public void startFight() {
        do {
            int first = (int) (Math.random() * warriors.size());
            int second = (int) (Math.random() * warriors.size());
            Warrior warrior1 = warriors.get(first);
            Warrior warrior2 = warriors.get(second);
            if (first != second) ;
            warrior1.attack(warrior2);
            if (warrior2.getHp()<=0);
            warriors.remove(second);
        }
        while (warriors.size()>1);
        System.out.println("***************\n Win - " + warriors.get(0) + "\n********************");
    }
}
