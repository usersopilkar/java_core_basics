package com.company;

public class Calendar {

    public enum Season {
        Spring, Summer, Autumn, Winter;
    }
        public enum Months {
            April(31, Season.Spring), May(31, Season.Spring), March(31, Season.Spring),
            December(31, Season.Winter), February(28, Season.Winter), January(31, Season.Winter),
            July(31, Season.Summer), June(30, Season.Summer), August(31, Season.Summer),
            November(30, Season.Autumn), October(30, Season.Autumn), September(30, Season.Autumn);
            int day;
            Season season;

            public int getDay() {
                return day;
            }

            public Season getSeason() {
                return season;
            }

            Months(int day, Season season) {
                this.day = day;
                this.season = season;
            }
        }
    }
