package com.company;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class EnergyTest {
    @Energy(volt = "very")
   public String name;
    @Energy(volt = "low")
   public int power;
    @Energy(volt = "high")
    public String color;
    public int length;
    @Energy(volt = "today")
    LocalDate today = LocalDate.now();
    @Energy(volt = "time")
    LocalTime time = LocalTime.now();
    @Energy(volt = "dateTime")
    LocalDateTime dateTime = LocalDateTime.now();

}
