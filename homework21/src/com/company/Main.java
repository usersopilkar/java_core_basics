package com.company;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        EnergyTest test = new EnergyTest();
        PrintWriter printWriter = null;
        try {
            printWriter = new PrintWriter("file_1");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Class<EnergyTest> energy = EnergyTest.class;
        final var time = test.time;
        final  var today =  test.today;
        final  var dateTime =  test.dateTime;
        System.out.println(dateTime);
        System.out.println(time);
        System.out.println(today);
        printWriter.println(dateTime);
        printWriter.println(time);
        printWriter.println(today);
        Field[] fields = energy.getFields();
        for (Field f : fields) {

            Annotation[] r = f.getAnnotations();
            System.out.println(f.getName());
            System.out.println(Arrays.toString(r));
            printWriter.println(f.getName());
            printWriter.println(Arrays.toString(r));

        }
        printWriter.close();
    }
}