package com.company;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Commodity {

    public static void main(String[] args) {
        List<Product> productList = new LinkedList<>();
        String s;

        Scanner scanner = new Scanner(System.in);
        for (int i =0;;i++){
            System.out.println("Enter what you need!");
            s = scanner.nextLine();

            switch (s) {
                case "addP":
                    productList.add(new Product(scanner.nextLine(), scanner.nextInt(), scanner.nextInt(), scanner.nextInt()));
                    break;
                case "removeP":
                    productList.remove(scanner);
                case "sortByName":
                    productList.sort(new NameComparator());
                case "sortByLength":
                    productList.sort(new LengthComparator());
                case "sortByWeight":
                    productList.sort(new WeightComparator());
                case "sortByWidth":
                    productList.sort(new WidthComparator());
                case "getList":
                   System.out.println(productList);
                case "exit":
                    System.exit(0);
            }

        }
    }
}

