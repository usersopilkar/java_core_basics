package com.company;

import java.util.Comparator;

public class WidthComparator implements Comparator<Product> {
    @Override
    public int compare(Product o1, Product o2) {
        return o1.getWidth()-o2.getWidth();
    }
}
