package com.company.zavd3;

public class Main {
    public static void main(String[] args) {
        String str = new String("I love Java more than my friend. Java is so beautiful");
        boolean matches = str.matches(".*Java.*");
        System.out.println(matches);
        String first = str.replaceFirst("Java", "C#");
        System.out.println(first);
        String all = str.replaceAll("Java", "C#");
        System.out.println(all);
    }
}
