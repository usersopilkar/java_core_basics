package com.company;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

public class Main {

    public static void main(String[] args) {
        Set<Monsters> monsters = new HashSet<>();
        monsters.add(new Monsters("Babaiko",40,"black" ));
        monsters.add(new Monsters("Vampire",453,"red"));
        monsters.add(new Monsters("Wolfich",32,"grey"));
        System.out.println(monsters);
        Set<Monsters> monster_sort = new TreeSet<>(monsters);
        System.out.println(monster_sort);
        Set<Monsters> monsters_link = new LinkedHashSet<>(monsters);
        System.out.println(monsters_link);
    }
}