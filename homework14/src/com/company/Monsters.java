package com.company;

public class Monsters implements Comparable<Monsters> {
    String name;
    int age;
    String color;

    public Monsters(String name, int age, String color) {
        this.name = name;
        this.age = age;
        this.color = color;
    }

    @Override
    public String toString() {
        return "Monsters{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", color='" + color + '\'' +
                '}';
    }

    @Override
    public int compareTo(Monsters monster1) {
        return this.name.compareTo(monster1.name);
    }
}
