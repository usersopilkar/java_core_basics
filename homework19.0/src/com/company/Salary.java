package com.company;

import java.io.Serializable;

public class Salary implements Serializable {
   private int salary;

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }
}
