package com.company;

import java.io.*;
import java.util.LinkedList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws Exception {
        Employee employee = new Employee("Sergiy",228,2000);
        Employee employee1 = new Employee("Rostyk",229,2500);
        Employee employee2 = new Employee("Artem",230,3000);
        List<Employee> list = new LinkedList<>();
        list.add(employee);
        list.add(employee1);
        list.add(employee2);

        for (Employee s: list) {
            s.serialize(s);
            s.deserialize(s);
        }

    }
}
