package com.company;

import java.io.*;

public class Employee implements Serializable {
    private String name;
    private int id;
    private transient int salary;

    public Employee(String name, int id, int salary) {
        this.name = name;
        this.id = id;
        this.salary = salary;
    }

    public Employee serialize(Employee o) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream("Employee");
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(o);
        objectOutputStream.close();
        return o;
    }

    public void deserialize(Employee o) throws IOException, ClassNotFoundException {
        FileInputStream fileInputStream = new FileInputStream("Employee");
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        Employee newEmployee = (Employee) objectInputStream.readObject();
        objectInputStream.close();
        System.out.println("Name - " + newEmployee.getName());
        System.out.println("ID - " + newEmployee.getId());
        System.out.println("Salary - " + newEmployee.getSalary());
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
