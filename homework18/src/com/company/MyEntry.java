package com.company;

import java.util.*;

public class MyEntry<K, V> {
    private K key;
    private V values;
    Map<K, V> map;

    @Override
    public String toString() {
        return "MyEntry{" +
                "key=" + key +
                ", values=" + values +
                ", map=" + map +
                '}';
    }

    public MyEntry(Map<K, V> map) {
        this.map = map;
    }

    public K getKey() {
        return key;
    }

    public void setKey(K key) {
        this.key = key;
    }

    public V getValues() {
        return values;
    }

    public void setValues(V values) {
        this.values = values;
    }

    public Map<K, V> getMap() {
        return map;
    }

    public void setMap(Map<K, V> map) {
        this.map = map;
    }

    public Set<K> getSetKey() {
        return map.keySet();
    }

    public Collection<V> getListVallues() {
        return map.values();
    }

    public K removeK(K key) {
        return (K) map.remove(key);
    }

    public Collection<V> removeV(V values) {
        return (Collection<V>) map.remove(values);
    }
}

